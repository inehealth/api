package api

import "encoding/json"

//Configuration struct
type Configuration struct {
	Host          string //ip address to listen to, use 0.0.0.0 to listen in all IP addresses
	HostName      string
	Port          uint16             //port to listen to
	WSPort        uint16             //ws port to listen to
	Prefix        string             //prefix to use in between the complete host name and the modules' endpoints i.e.: http://127.0.0.1:9292/prefix/...
	SSLPrivateKey string             //path to a SSL private key setting this property enables SSL Mode on the API
	SSLPublicKey  string             //path to a SSL public key
	StrictSlash   bool               //Enforces the use of a trailing final slash '/' in all the URLs
	Dispatcher    DispatcherConfig   //in case of use,identifier
	FileStorage   *FileStorageConfig //in case of use,identifier
}

//FileStorageConfig struct
type FileStorageConfig struct {
	Type string      //const used to declare the type of the service storage
	Data interface{} //representation of the attributes
}

//DispatcherConfig struct
type DispatcherConfig struct {
	Type string      //const used to declare the type of the dispatcher, by default "async"
	Data interface{} //representation of the attributes
}

//NewConfigurationFromJSON creates and returns a Configuration Object from a JSON string
func NewConfigurationFromJSON(config string) (configuration *Configuration, err error) {
	err = json.Unmarshal([]byte(config), &configuration)
	return configuration, err
}

//NewConfiguration creates and returns a Configuration Object from a given host and port
func NewConfiguration(host string, port uint16) (configuration *Configuration, err error) {
	configuration.Host = host
	configuration.Port = port

	return configuration, nil
}
