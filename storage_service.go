package api

import (
	"archive/zip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
)

//StorageService interface of storage service
type StorageService interface {
	Write(data []byte, path string) (identifier string, err error)
	Read(from ...string) (data []byte, err error)
	Delete(from ...string) error
}

//FilesystemSSType is the identifier of the Filesystem storage type
const FilesystemSSType = "filesystem"

//PathPermissions ...
const PathPermissions = 0755

//FilePermission ...
const FilePermission = 0644

//FilesystemStorageService ...
type FilesystemStorageService struct {
	basePath string
}

//NewFilesystemStorageService ...
func NewFilesystemStorageService(bPath string) *FilesystemStorageService {

	return &FilesystemStorageService{
		basePath: bPath,
	}
}

//Write USE ONLY WITH storageService. Writes in the filesystem.
func (fss FilesystemStorageService) Write(data []byte, destination string) (identifier string, err error) {

	if len(data) == 0 {
		err = errors.New("empty data")
		return
	}

	completePath := path.Join(fss.basePath, destination)
	identifier = uuid.New().String()

	if _, err := os.Stat(completePath); os.IsNotExist(err) {
		err = os.MkdirAll(completePath, PathPermissions)
		if err != nil {
			return "", err
		}
		log.Printf("Path %s created", completePath)
	}

	err = ioutil.WriteFile(path.Join(completePath, identifier), data, FilePermission)
	return
}

//Read the filesystem
func (fss FilesystemStorageService) Read(from ...string) (data []byte, err error) {

	return ioutil.ReadFile(path.Join(fss.basePath, strings.Join(from, string(os.PathSeparator))))
}

//Delete on the filesystem
func (fss FilesystemStorageService) Delete(from ...string) error {

	return os.RemoveAll(path.Join(fss.basePath, strings.Join(from, string(os.PathSeparator))))
}

//GetBasePath return the base path configured
func (fss FilesystemStorageService) GetBasePath() string {
	return fss.basePath
}

//MkdirAll builds directories with needed parents for a given path
func (fss FilesystemStorageService) MkdirAll(directories ...string) (err error) {

	fullPath := path.Join(fss.basePath, strings.Join(directories, string(os.PathSeparator)))
	if _, err := os.Stat(fullPath); os.IsNotExist(err) {
		os.MkdirAll(fullPath, os.ModePerm)
	}

	return
}

//WriteFile USE ONLY WITH localStorage. Writes a file in the local system
func (fss FilesystemStorageService) WriteFile(data []byte, destination string) (err error) {

	if len(data) == 0 {
		err = errors.New("empty data")
		return
	}

	completePath := path.Join(fss.basePath, destination)
	locationPath, fileName := path.Split(destination)

	if fileName == "" {
		err = errors.New("Filename must be setted")
		return
	}

	fss.MkdirAll(locationPath)

	err = ioutil.WriteFile(completePath, data, FilePermission)
	return
}

//WriteJSON writes a json file from an interface
func (fss FilesystemStorageService) WriteJSON(item interface{}, destination string) (err error) {
	data, err := json.Marshal(item)
	if err != nil {
		return
	}

	if !strings.HasSuffix(destination, ".json") {
		destination = destination + ".json"
	}

	err = fss.WriteFile(data, destination)

	return
}

//Zip compress source to destination
func (fss FilesystemStorageService) Zip(source string, destination string) (err error) {
	source = path.Join(fss.basePath, source)
	destination = path.Join(fss.basePath, destination)

	zipfile, err := os.Create(destination)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	info, err := os.Stat(source)
	if err != nil {
		return nil
	}

	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}

	filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		if baseDir != "" {
			header.Name = filepath.Join(baseDir, strings.TrimPrefix(path, source))
		}

		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}

		writer, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(writer, file)
		return err
	})

	return
}

// CopyFile copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file. The file mode will be copied from the source and
// the copied data is synced/flushed to stable storage.
func (fss FilesystemStorageService) CopyFile(src string, dst string) (err error) {
	if !strings.HasPrefix(src, fss.basePath) {
		src = path.Join(fss.basePath, src)
	}
	if !strings.HasPrefix(dst, fss.basePath) {
		dst = path.Join(fss.basePath, dst)
	}

	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		if e := out.Close(); e != nil {
			err = e
		}
	}()

	_, err = io.Copy(out, in)
	if err != nil {
		return
	}

	err = out.Sync()
	if err != nil {
		return
	}

	si, err := os.Stat(src)
	if err != nil {
		return
	}
	err = os.Chmod(dst, si.Mode())
	if err != nil {
		return
	}

	return
}

// CopyDir recursively copies a directory tree, attempting to preserve permissions.
// Source directory must exist, destination directory must *not* exist.
// Symlinks are ignored and skipped.
func (fss FilesystemStorageService) CopyDir(src string, dst string) (err error) {
	if !strings.HasPrefix(src, fss.basePath) {
		src = path.Join(fss.basePath, src)
	}
	if !strings.HasPrefix(dst, fss.basePath) {
		dst = path.Join(fss.basePath, dst)
	}

	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return fmt.Errorf("source is not a directory")
	}

	_, err = os.Stat(dst)
	if err != nil && !os.IsNotExist(err) {
		return
	}
	if err == nil {
		return fmt.Errorf("destination already exists")
	}

	err = os.MkdirAll(dst, si.Mode())
	if err != nil {
		return
	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = fss.CopyDir(srcPath, dstPath)
			if err != nil {
				return
			}
		} else {
			// Skip symlinks.
			if entry.Mode()&os.ModeSymlink != 0 {
				continue
			}

			err = fss.CopyFile(srcPath, dstPath)
			if err != nil {
				return
			}
		}
	}

	return
}
