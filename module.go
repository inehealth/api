package api

import "net/http"

//Endpoint represents an endpoint path and a collection of possible http methods to be used on that endpoint
type Endpoint struct {
	Path    string
	Methods []string //ideally only http.Method* constants should be used
}

//Endpoints maps an endpoint to a given handler of the form func(w http.ResponseWriter, r *http.Request)
type Endpoints map[*Endpoint]func(w http.ResponseWriter, r *http.Request)

//Module is the interface that defines a compatible Module
type Module interface {
	SetDependencies(Resources)
	GetEndpoints() Endpoints
	GetListeners() map[string][]Listener
}

//RegisterModule adds any given quantity of Module items to the api
func (api *API) RegisterModule(modules ...Module) {
	api.modules = append(api.modules, modules...)
}
