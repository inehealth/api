package api

import (
	"log"
	"os"
	"testing"
	"time"
)

type LiestenerTest struct {
	processed bool
}

func (l *LiestenerTest) Process(topic string, event Event) {
	l.processed = true
}

func TestFunciontalInitAsyncEventDispatcher(t *testing.T) {

	d := AsyncDispatcher{}

	if d.isInitialized {
		t.Error("For", d, "expected initizalized", false, "got", d.isInitialized)
	}

	err := d.register("test", &LiestenerTest{})
	if err != ErrAsyncDispatcherNotInitialized {
		t.Error("For not initialized dispatcher", d, "expected", ErrAsyncDispatcherNotInitialized, "got", err)
	}

	d.Init(log.New(os.Stdout, "[test]", 0))
	if !d.isInitialized {
		t.Error("For", d, "expected initizalized", true, "got", d.isInitialized)
	}
	if d.Logger == nil {
		t.Error("For", d, "expected", "not nil", "got", d.Logger)
	}
	if len(d.Listeners) > 0 {
		t.Error("For", d, "expected topic listeners", 0, "got", len(d.Listeners))
	}

}

func TestUnitRegisterAsyncEventDispatcher(t *testing.T) {

	d := AsyncDispatcher{}
	d.Init(log.New(os.Stdout, "[test]", 0))

	//Register one listener on "test1" topic
	d.register("test1", &LiestenerTest{})
	if len(d.Listeners) != 1 {
		t.Error("For", d, "expected topic listeners", 1, "got", len(d.Listeners))
	}
	if _, ok := d.Listeners["test1"]; !ok {
		t.Error("For", d, "expected topic", "test1", "got", nil)
	}
	if len(d.Listeners["test1"]) != 1 {
		t.Error("For", d, "expected topic test1 count", 1, "got", len(d.Listeners["test1"]))
	}

	//Register two more listener on "test1" topic
	listeners := []Listener{&LiestenerTest{}, &LiestenerTest{}}
	d.register("test1", listeners...)
	if len(d.Listeners) != 1 {
		t.Error("For", d, "expected topic listeners", 1, "got", len(d.Listeners))
	}
	if len(d.Listeners["test1"]) != 3 {
		t.Error("For", d, "expected topic test1 count", 3, "got", len(d.Listeners["test1"]))
	}

	//Register another listener on distinct topic
	d.register("test2", &LiestenerTest{})
	if len(d.Listeners) != 2 {
		t.Error("For", d, "expected topic listeners", 2, "got", len(d.Listeners))
	}
	if _, ok := d.Listeners["test2"]; !ok {
		t.Error("For", d, "expected topic", "test2", "got", nil)
	}
}

func TestFunciontalAsyncEventDispatcher(t *testing.T) {

	listener := &LiestenerTest{}

	d := AsyncDispatcher{}
	d.Init(log.New(os.Stdout, "[test]", 0))
	d.register("testable", listener)

	d.Dispatch("invented", "")

	if listener.processed {
		t.Error("For", listener, "expected", "not processed", "got", listener.processed)
	}

	d.Dispatch("testable", "")
	//wait for async calls...
	time.Sleep(1 * time.Millisecond)
	if !listener.processed {
		t.Error("For", listener, "expected", "processed", "got", listener.processed)
	}

}
