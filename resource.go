package api

//ResourceObject represents a dependency, an object to be injected to the modules.
type ResourceObject interface{}

//Resource is a ResourceObject with a given unique name, an ID
type Resource struct {
	Name   string
	Object ResourceObject
}

//Resources is a collection of ResourceObjects in a map
type Resources map[string]ResourceObject

//RegisterResource adds any given quantity of resources to the API's dependency container
func (api *API) RegisterResource(resources ...Resource) {
	for _, resource := range resources {
		api.dependencyContainer[resource.Name] = resource.Object
	}
}
