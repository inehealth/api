# api
--
    import "bitbucket.org/inehealth/api"

Package api provides a collection of interfaces, types and structs to help in
the development of public APIs for Inehealth.

## Usage

```go
var ErrAsyncDispatcherNotInitialized = errors.New("AsyncDispatcher is not initialized")
```
ErrAsyncDispatcherNotInitialized generic error AsyncDispatcher not initialized

#### func  JSONResponse

```go
func JSONResponse(response interface{}, w http.ResponseWriter, statusCode int)
```
JSONResponse helper to print a json response

#### func  XMLResponse

```go
func XMLResponse(response interface{}, w http.ResponseWriter, statusCode int, selfClosingTags []string)
```
XMLResponse helper to print a xml response

#### type API

```go
type API struct {
	Config *Configuration
	Logger *log.Logger
}
```

API ...

#### func  NewAPI

```go
func NewAPI(config *Configuration) *API
```
NewAPI creates and returns a new API object with a given configuration

#### func (*API) RegisterModule

```go
func (api *API) RegisterModule(modules ...Module)
```
RegisterModule adds any given quantity of Module items to the api

#### func (*API) RegisterResource

```go
func (api *API) RegisterResource(resources ...Resource)
```
RegisterResource adds any given quantity of resources to the API's dependency
container

#### func (*API) Start

```go
func (api *API) Start(middlewares map[string]func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)) (err error)
```
Start initializes all the modules and runs negroni using the API configuration

#### type AsyncDispatcher

```go
type AsyncDispatcher struct {
	Listeners map[string][]Listener
	Logger    *log.Logger
}
```

AsyncDispatcher is a concrete representation of the EventDispatcher. It aims to
call listeners asynchronously

#### func (*AsyncDispatcher) Dispatch

```go
func (ad *AsyncDispatcher) Dispatch(topic string, event Event)
```
Dispatch does a loop over the listeners and launches those stored behind topic
passed as parameter Calls to the process method in async way

#### func (*AsyncDispatcher) Init

```go
func (ad *AsyncDispatcher) Init(logger *log.Logger)
```
Init initializes the map of listeners, the logger and set the isInitialize flag
to true

#### type Configuration

```go
type Configuration struct {
	Host          string //ip address to listen to, use 0.0.0.0 to listen in all IP addresses
	Port          uint16 //port to listen to
	Prefix        string //prefix to use in between the complete host name and the modules' endpoints i.e.: http://127.0.0.1:9292/prefix/...
	SSLPrivateKey string //path to a SSL private key setting this property enables SSL Mode on the API
	SSLPublicKey  string //path to a SSL public key
	StrictSlash   bool   //Enforces the use of a trailing final slash '/' in all the URLs
}
```

Configuration struct

#### func  NewConfiguration

```go
func NewConfiguration(host string, port uint16) (configuration *Configuration, err error)
```
NewConfiguration creates and returns a Configuration Object from a given host
and port

#### func  NewConfigurationFromJSON

```go
func NewConfigurationFromJSON(config string) (configuration *Configuration, err error)
```
NewConfigurationFromJSON creates and returns a Configuration Object from a JSON
string

#### type Endpoint

```go
type Endpoint struct {
	Path    string
	Methods []string //ideally only http.Method* constants should be used
}
```

Endpoint represents an endpoint path and a collection of possible http methods
to be used on that endpoint

#### type Endpoints

```go
type Endpoints map[*Endpoint]func(w http.ResponseWriter, r *http.Request)
```

Endpoints maps an endpoint to a given handler of the form func(w
http.ResponseWriter, r *http.Request)

#### type Event

```go
type Event interface{}
```

Event is the interface of the events for the EventDispatcher

#### type EventDispatcher

```go
type EventDispatcher interface {
	Dispatch(topic string, event Event)
	// contains filtered or unexported methods
}
```

EventDispatcher is the interface of the dispatcher for the API

#### type Listener

```go
type Listener interface {
	// contains filtered or unexported methods
}
```

Listener is the interface that must to accomplish whom want to use the event
dispatcher

#### type Module

```go
type Module interface {
	SetDependencies(Resources)
	GetEndpoints() Endpoints
	GetListeners() map[string][]Listener
}
```

Module is the interface that defines a compatible Module

#### type Resource

```go
type Resource struct {
	Name   string
	Object ResourceObject
}
```

Resource is a ResourceObject with a given unique name, an ID

#### type ResourceObject

```go
type ResourceObject interface{}
```

ResourceObject represents a dependency, an object to be injected to the modules.

#### type Resources

```go
type Resources map[string]ResourceObject
```

Resources is a collection of ResourceObjects in a map
