package api

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/url"
	"reflect"

	"cloud.google.com/go/pubsub"
	"github.com/google/uuid"
)

//DispatcherGCPPubSubType is the identifier of the google cloud platform pubsub service
const DispatcherGCPPubSubType = "gcp-pubsub"

//Event is the interface of the events for the EventDispatcher
type Event interface{}

//PubSubEvent is a wrapper to add some extra data to the default event
type PubSubEvent struct {
	Type       string        `json:"type"`       //notification type
	Identifier string        `json:"identifier"` //notification identifier
	To         []string      `json:"to"`
	Payload    PayloadPubSub `json:"payload"`
	TTL        *int          `json:"ttl,omitempty"` //time to live in seconds (for immediate processing)
}

//PayloadPubSub is the data passed to the clients by the subscribers
type PayloadPubSub struct {
	Options      PubSubOptions       `json:"options"`
	Notification *PubSubNotification `json:"notification,omitempty"`
	Data         interface{}         `json:"data"`
}

//PubSubOptions is the config for the client of the PubSubEvent
type PubSubOptions struct {
	Collapse    bool    `json:"collapse"`
	CollapseKey *string `json:"collapse_key,omitempty"`
	Persisted   bool    `json:"persisted"`
	Priority    int     `json:"priority"`
	TTL         uint32  `json:"ttl"` //Time To Live on seconds
}

//PubSubNotification content the visual data of an user's notification
type PubSubNotification struct {
	Title string   `json:"title"`
	Body  string   `json:"body"`
	Icon  *string  `json:"icon"`
	URL   *url.URL `json:"url"`
}

//Listener is the interface that must to accomplish whom want to use the event dispatcher
type Listener interface {
	Process(topic string, event Event)
}

//EventDispatcher is the interface of the dispatcher for the API
type EventDispatcher interface {
	register(topic string, listeners ...Listener) (err error)
	Dispatch(topic string, event Event)
}

//////////////////////////// Async Dispatcher ////////////////////////////

//AsyncDispatcher is a concrete representation of the EventDispatcher.
//It aims to call listeners asynchronously
type AsyncDispatcher struct {
	Listeners     map[string][]Listener
	Logger        *log.Logger
	isInitialized bool
}

//PubSubDispatcher queues into google pub/sub service the payload of the data received
type PubSubDispatcher struct {
	client *pubsub.Client
	logger *log.Logger
}

//FakeDispatcher is a concrete representation for tests
type FakeDispatcher struct{}

//ErrAsyncDispatcherNotInitialized generic error AsyncDispatcher not initialized
var ErrAsyncDispatcherNotInitialized = errors.New("AsyncDispatcher is not initialized")

//Init initializes the map of listeners, the logger and set the isInitialize flag to true
func (ad *AsyncDispatcher) Init(logger *log.Logger) {

	ad.Listeners = make(map[string][]Listener)
	ad.Logger = logger
	ad.isInitialized = true

}

//Register stores the listener into listeners map
func (ad *AsyncDispatcher) register(topic string, listeners ...Listener) (err error) {

	if !ad.isInitialized {
		return ErrAsyncDispatcherNotInitialized
	}

	if _, ok := ad.Listeners[topic]; !ok {
		ad.Listeners[topic] = []Listener{}
	}

	ad.Listeners[topic] = append(ad.Listeners[topic], listeners...)
	ad.Logger.Printf("Registered %d listener to the topic: %s", len(listeners), topic)

	return nil
}

//Dispatch does a loop over the listeners and launches those stored behind topic passed as parameter
//Calls to the process method in async way
func (ad *AsyncDispatcher) Dispatch(topic string, event Event) {

	if _, ok := ad.Listeners[topic]; !ok {
		ad.Logger.Printf("Not listeners registered for '%s' topic", topic)
		return
	}

	for _, listener := range ad.Listeners[topic] {
		go listener.Process(topic, event)
		ad.Logger.Printf("Launched event %s for the listener %s", topic, reflect.TypeOf(listener).String())
	}

}

///////////////////// Google Pub/Sub dispatcher ////////////////////////////

//Init ...
func (psd *PubSubDispatcher) Init(conf map[string]interface{}, logger *log.Logger) (err error) {

	if _, ok := conf["ProjectID"]; !ok {
		return errors.New("you must define a ProjectID attribute on conf")
	}

	psd.logger = logger
	psd.client, err = pubsub.NewClient(context.Background(), conf["ProjectID"].(string))

	return

}

//Register do nothing
func (psd *PubSubDispatcher) register(topic string, listeners ...Listener) (err error) {
	//do nothing
	return
}

//Dispatch pushes into the topic the event received. Resource limits:
//	Message size (the data field): 10MB (the maximum request size)
//	Attributes per message: 100
//	Attribute key size: 256 bytes
//	Attribute value size: 1024 bytes
func (psd *PubSubDispatcher) Dispatch(topic string, event Event) {

	var err error

	gcpTopic := psd.client.Topic(topic)
	exists, _ := gcpTopic.Exists(context.Background())
	if !exists {
		gcpTopic, err = psd.client.CreateTopic(context.Background(), topic)
		if err != nil {
			psd.logger.Printf("failed on topic %s creation: %s", topic, err.Error())
			return
		}
		psd.logger.Printf("topic %s created", topic)
	}

	data, _ := json.Marshal(event)
	msg := &pubsub.Message{
		Attributes: map[string]string{"uuid": uuid.New().String()},
		Data:       data,
	}

	gcpTopic.Publish(context.Background(), msg)
	gcpTopic.Stop()

	psd.logger.Printf("event pushed on pub/sub to topic %s", topic)

}

///////////////////// Dispatcher for tests ////////////////////////////

//Register do nothing
func (ad *FakeDispatcher) register(topic string, listeners ...Listener) (err error) {
	return
}

//Dispatch do nothing
func (ad *FakeDispatcher) Dispatch(topic string, event Event) {

}
