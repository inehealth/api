package api

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

//JSONResponse helper to print a json response
func JSONResponse(response interface{}, w http.ResponseWriter, statusCode int) {

	data, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("JSON RESPONSE ERROR: %s", err.Error())
		return
	}
	if statusCode <= 0 {
		statusCode = http.StatusOK
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(data)))
	w.WriteHeader(statusCode)
	w.Write(data)
}

//XMLResponse helper to print a xml response
func XMLResponse(response interface{}, w http.ResponseWriter, statusCode int, selfClosingTags []string) {

	data, err := xml.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var prefix string
	refType := reflect.ValueOf(response).Elem()
	refField := refType.FieldByName("XmlVersion")
	if refField.IsValid() {
		prefix += refField.String() + "\n"
	}
	refField = refType.FieldByName("DocType")
	if refField.IsValid() {
		prefix += refField.String() + "\n"
	}
	if len(selfClosingTags) > 0 {
		r := regexp.MustCompile(fmt.Sprintf("></(%s)>", strings.Join(selfClosingTags, "|")))
		data = []byte(r.ReplaceAllString(string(data), "/>"))
	}
	if len(prefix) > 0 {
		data = append([]byte(prefix), data...)
	}
	if statusCode <= 0 {
		statusCode = http.StatusOK
	}

	w.Header().Set("Content-Type", "application/xml")
	w.WriteHeader(statusCode)
	w.Write(data)
}
