//Package api provides a collection of interfaces, types and structs to help in the development of public APIs for Inehealth.
package api

import (
	"context"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

//API ...
type API struct {
	Config              *Configuration
	Logger              *log.Logger
	router              *mux.Router
	negroni             *negroni.Negroni
	eventDispatcher     EventDispatcher //stored as a pointer
	storageService      StorageService  //stored as a pointer
	modules             []Module
	dependencyContainer Resources
	server              *http.Server
}

//LoggerWriter ...
var LoggerWriter io.Writer = os.Stdout

//NewAPI creates and returns a new API object with a given configuration
func NewAPI(config *Configuration) *API {

	api := API{}

	api.Config = config
	api.Logger = log.New(LoggerWriter, "[ api ] ", log.LstdFlags|log.LUTC|log.Lshortfile)
	api.dependencyContainer = make(map[string]ResourceObject)
	api.router = mux.NewRouter().StrictSlash(api.Config.StrictSlash)

	if api.Config.FileStorage != nil {
		switch api.Config.FileStorage.Type {
		case FilesystemSSType:
			api.storageService = NewFilesystemStorageService(api.Config.FileStorage.Data.(string))
		default:
			api.Logger.Printf("ERROR: no type %s of file storage defined", api.Config.FileStorage.Type)
		}
	}

	switch config.Dispatcher.Type {
	case DispatcherGCPPubSubType:

		dispatcherData, ok := config.Dispatcher.Data.(map[string]interface{})
		if !ok {
			api.Logger.Fatalf("Error trying to extract the dispatcher data: %#v", config.Dispatcher.Data)
		}

		dispatcher := new(PubSubDispatcher)
		err := dispatcher.Init(dispatcherData, api.Logger)
		if err != nil {
			api.Logger.Fatalf("Error creating the dispatcher of %s type: %s", DispatcherGCPPubSubType, err.Error())
		}
		api.eventDispatcher = dispatcher
		api.Logger.Printf("Created and initialized gcp dispatcher")
	default:
		dispatcher := new(AsyncDispatcher)
		dispatcher.Init(api.Logger)
		api.eventDispatcher = dispatcher
		api.Logger.Printf("Created and initialized AsyncDispatcher")
	}

	return &api
}

//GetEventDispatcher returns the event dispatcher created on NewAPI function
func (api API) GetEventDispatcher() EventDispatcher {
	return api.eventDispatcher
}

//GetStorageService returns the storage service created on NewAPI function
func (api API) GetStorageService() StorageService {
	return api.storageService
}

//PrepareFileServer prepares an endpoint to retrieve static files
//Optional, is not necessary call for its correct behaviourci
func (api *API) PrepareFileServer(prefix string, path string) {

	api.router.PathPrefix(prefix).Handler(
		http.StripPrefix(prefix, http.FileServer(http.Dir(path))),
	)

	api.Logger.Printf("FILE SERVER: Added endpoint for prefix %s on path %s", prefix, path)

}

//Start initializes all the modules and runs negroni using the API configuration
func (api *API) Start(middlewares map[string]func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)) (err error) {

	api.negroni = negroni.New(negroni.NewRecovery())
	for k, m := range middlewares {
		api.negroni.UseFunc(m)
		api.Logger.Printf("Middleware %s registered", k)
	}
	api.negroni.UseHandler(api.router)

	//Adds the EventDispatcher as a modules dependencies
	api.dependencyContainer["dispatcher"] = api.eventDispatcher
	api.dependencyContainer["logger"] = api.Logger
	api.dependencyContainer["filestorage"] = api.storageService

	port := strconv.Itoa(int(api.Config.Port))
	addr := api.Config.Host + ":" + port

	for _, module := range api.modules {
		if i, ok := module.(interface {
			Init(resources Resources)
		}); ok {
			i.Init(api.dependencyContainer)
		}

		module.SetDependencies(api.dependencyContainer)
		for endpoint, handler := range module.GetEndpoints() {

			urlPath := endpoint.Path
			//if the prefix has length and the endpoint starts without slash we put the prefix on the beginning
			if len(api.Config.Prefix) > 0 && !strings.HasPrefix(endpoint.Path, "/") {
				urlPath = api.Config.Prefix + "/" + endpoint.Path
			}

			api.Logger.Printf("registering endpoint %s for http methods %v", urlPath, endpoint.Methods)
			api.router.HandleFunc(urlPath, handler).Methods(endpoint.Methods...)

		}

		//the listeners could use the previous dependencies
		for topic, listeners := range module.GetListeners() {
			api.eventDispatcher.register(topic, listeners...)
		}

	}

	api.server = &http.Server{Addr: addr, Handler: api.negroni}

	go func() {
		if len(api.Config.SSLPrivateKey) > 0 {
			api.Logger.Printf("listening using SSL on %s \n", addr)
			api.Logger.Fatal(api.server.ListenAndServeTLS(api.Config.SSLPublicKey, api.Config.SSLPrivateKey))
		} else {
			api.Logger.Printf("listening unsecured on %s \n", addr)
			api.Logger.Fatal(api.server.ListenAndServe())
		}
	}()

	return
}

//Stop stops the api
func (api *API) Stop() {

	api.Logger.Println("Gracefully shutting down")

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)

	defer cancel()

	api.server.Shutdown(ctx)

	api.Logger.Println("Server Stopped")

	return
}
